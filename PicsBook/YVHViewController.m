//
//  YVHViewController.m
//  PicsBook
//
//  Created by Yvon Valdepeñas on 04/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import "YVHViewController.h"
#import "YVHCameraController.h"

@interface YVHViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *photo;
- (IBAction)takePhoto:(id)sender;

@end

@implementation YVHViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}



- (IBAction)takePhoto:(id)sender {
    
    YVHCameraController* camera = [YVHCameraController new];
    [camera takePhoto];
}




#pragma mark - YVHCameraControllerDelegate methods
-(void) cameraController:(YVHCameraController*)camerController photoTook:(UIImage*)photo{
    self.photo.image = photo;
}

@end