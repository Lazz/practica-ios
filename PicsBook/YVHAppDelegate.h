//
//  YVHAppDelegate.h
//  PicsBook
//
//  Created by Yvon Valdepeñas on 04/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YVHAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
