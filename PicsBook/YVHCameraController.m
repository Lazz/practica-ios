//
//  YVHCameraController.m
//  PicsBook
//
//  Created by Yvon Valdepeñas on 04/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import "YVHCameraController.h"

@interface YVHCameraController ()

@property (strong, nonatomic)  UIImageView *photo;


@end

@implementation YVHCameraController

-(void)viewDidLoad{

}


-(void)takePhoto{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        picker.showsCameraControls = YES;
        picker.allowsEditing = YES;
        
        
        [self presentViewController:picker animated:YES completion:nil];
     
    }
    else {
        [[[UIAlertView alloc] initWithTitle:@"Error" message:@"No uses el simulador, cacho bestia" delegate:nil cancelButtonTitle:@"Vale, vale" otherButtonTitles: nil] show];
    }
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalPhoto = [info objectForKey:UIImagePickerControllerOriginalImage];
    //self.photo.image = originalPhoto;
    [self.delegate cameraController:self photoTook:originalPhoto];
    
    
    UIImageWriteToSavedPhotosAlbum(originalPhoto, nil, NULL, NULL);
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
@end
