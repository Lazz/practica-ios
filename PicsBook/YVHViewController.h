//
//  YVHViewController.h
//  PicsBook
//
//  Created by Yvon Valdepeñas on 04/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YVHCameraController.h"

@interface YVHViewController : UIViewController<YVHCameraControllerDelegate>

@end
