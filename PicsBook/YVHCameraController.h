//
//  YVHCameraController.h
//  PicsBook
//
//  Created by Yvon Valdepeñas on 04/04/14.
//  Copyright (c) 2014 Yvon Valdepeñas. All rights reserved.
//

#import <UIKit/UIKit.h>

@class  YVHCameraController;

@protocol YVHCameraControllerDelegate

-(void) cameraController:(YVHCameraController*)camerController photoTook:(UIImage*)photo;

@end

@interface YVHCameraController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (weak, nonatomic) id<YVHCameraControllerDelegate> delegate;

-(void)takePhoto;

@end
